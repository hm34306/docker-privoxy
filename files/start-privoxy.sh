#!/bin/bash

# build the block list
./privoxy-blocklist.sh

# start as service - wnat to see logs --no-daemon does not allow this
/usr/sbin/privoxy /etc/privoxy/config

ps -ef | grep /usr/sbin/privoxy

echo "Testing ps -ef | grep -ic /usr/sbin/privoxy"
match=$(ps -ef | grep -ic /usr/sbin/privoxy)
echo $match
if  [ $match -eq 2 ]
then
    echo "privoxy is running"
else
    echo "privoxy process is not running... EXITING"
    exit 1
fi

echo "Testing netstat -plnt | grep -ic 8118"
match=$(netstat -plnt | grep -ic 8118)
echo $match
if  [ $match -eq 1 ]
then
    echo "privoxy is running on port 8118"
    echo "running ping to keep container alive... COMPLETE"
    # ping to keep alive... kind of hacky
    ping localhost
else
    echo "privoxy process is not running on port 8118... EXITING"
    exit 1
fi


