FROM jjanzic/docker-privoxy
LABEL MAINTAINER Hector Menchaca <hector-menchaca@hotmail.com>

# Args for Open DNS set up
ENV RESOLV_CONF=/etc/resolvconf/resolv.conf.d/base \
    OPEN_DNS_1=208.67.222.222 \
    OPEN_DNS_2=208.67.220.220

# do not want to run a daemon
COPY files/start-privoxy.sh /start-privoxy.sh

# Adding custom filters
# TODO: autobuild filters for XXX content
COPY files/default.action /etc/privoxy/default.action
COPY files/default.filter /etc/privoxy/default.filter

RUN echo "logfile logfile\ndebug 512" >> /etc/privoxy/config && \
    echo "nameserver $OPEN_DNS_1" >> $RESOLV_CONF && \
    echo "nameserver $OPEN_DNS_2" >> $RESOLV_CONF

# Run.sh from jjanzic/docker-privoxy
CMD ["/start-privoxy.sh"]