import unittest
import time
from subprocess import check_call, check_output, call

"""
Executor must have passwordless sudo
"""


# Docker Properties
CONTAINER_NAME = "privoxy"


# Docker Helper functions
def _docker():
    """Generic sudo docker command"""
    # return ["sudo", "docker"]
    return ["docker"]


def _docker_exec():
    """Generic docker exec command"""
    return _docker() + ["exec", CONTAINER_NAME]


def docker_call(args):
    """Generic docker command apending args"""
    check_call(_docker() + args)


def docker_output(args):
    """Generic docker command apending args"""
    return check_output(_docker() + args)


def docker_exec(args):
    """Generic docker exec command appending args"""
    return check_output(_docker_exec() + args)


# TEST Properties
RESOLV_CONF = "/etc/resolvconf/resolv.conf.d/base"
PORT = "8118"
OPEN_DNS_1 = "208.67.222.222"
OPEN_DNS_2 = "208.67.220.220"


class TestDockerFile(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        # check_call(["sudo", "./docker-run-privoxy.sh"])
        check_call(["docker-compose", "up", "-d"])
        check_call(["./wait-until-ready.sh"])

    @classmethod
    def tearDownClass(self):
        check_call(["docker-compose", "down"])

    """
    def test_start_complete(self):
        output = docker_output(["logs", CONTAINER_NAME])
        while '... COMPLETE' not in output:
            print output
            time.sleep(1)
        assert '... COMPLETE' in output, "Container did not start correctly"
    """

    def test_port(self):
        output = check_output(["netstat", "-plnt", "|", "grep", PORT])
        print output
        assert ":8118" in output, "Port 8118 is not available"

    def test_open_dns(self):
        output = docker_exec(["sudo", "cat", RESOLV_CONF])
        print output
        assert OPEN_DNS_1 in output
        assert OPEN_DNS_2 in output


if __name__ == '__main__':
    unittest.main()
