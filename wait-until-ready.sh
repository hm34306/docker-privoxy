#!/bin/bash
# docker-run-privoxy
##
## FILE: docker-run-privoxy.sh
##
## DESCRIPTION: Safe run script for docker-run-privoxy
##
## AUTHOR: Hector Menchaca
##
## DATE: 2017-12-20
## 
## VERSION: 1.0
##
## USAGE: docker-run-privoxy
##
## RIGHTS: As is, free to use
##

NAME=privoxy
SOURCE_LOG_DIR=/var/log/$NAME
SOURCE_LOG_FILE=$SOURCE_LOG_DIR/logfile
LOG_FILE=/var/log/$NAME/logfile

# functions

init_log_dirs(){
    echo "Creating $SOURCE_LOG_DIR"
    mkdir -p $SOURCE_LOG_DIR
    echo "chmod 777 $SOURCE_LOG_DIR"
    chmod 777 $SOURCE_LOG_DIR
    echo "touch $SOURCE_LOG_FILE"
    touch $SOURCE_LOG_FILE
    chmod 777 $SOURCE_LOG_FILE
}

fail_check(){
    match=$(docker logs $NAME | grep -ic '... FAILED')
    if  [ $match -gt 0 ]
    then    
        docker_failed_to_start
        return 1
    fi

    return 0
}

success_check(){
    
    match=$(docker logs $NAME | grep -ic '... COMPLETE')
    if  [ $match -gt 0 ]
    then
        docker_suceeded
        return 1
    fi

    return 0
}

docker_failed_to_start(){
    echo "docker $NAME container failed to start"
    destroy_container
}

docker_suceeded(){  
    echo "docker $NAME container started successfully"    
}

root_check()
{
    is_root=$(whoami)
    if [ "$is_root" != "root" ]
    then 
        echo "Command must run with root or using sudo"
        exit
    fi
}

#main
root_check

init_log_dirs

counter="0"

while [ $counter -lt 12 ]
do
    fail_check
    if [ $? -eq 1 ]
    then
        break
    fi

    success_check
    if [ $? -eq 1 ]
    then
        break
    fi

    counter=$[$counter+1]
    echo "docker container not ready... will check again in 5 seconds"
    sleep 5
done

# Check if timeout has been exceeded
if  [ $counter -eq 5 ]
then    
    docker logs privoxy
    echo "docker $NAME container failed to start after 60 seconds"
    destroy_container
fi

echo "-------------------------------------------------"
echo "privoxy log file can be found at $SOURCE_LOG_FILE"